from flask_restful import Resource, reqparse
from flask import request
from models.company import CompanyModel
from models.product import ProductModel


class CompanyProducts(Resource):
    @property
    def data(self):
        payload = reqparse.RequestParser()
        payload.add_argument(
            'company_id',
            type=str,
            required=False,
            help='No company_id provided',
            location='json'
        )
        payload.add_argument(
            'products',
            type=list,
            required=False,
            help='No products provided',
            location='json'
        )
        return payload.parse_args()

    def get(self):
        params = request.args.get('company_id', None)

        if params is not None:
            companies = CompanyModel.query.filter_by(company_id=params)
        else:
            companies = CompanyModel.query.order_by(CompanyModel.company_id).all()

        output = {
            "companies": [c.json() for c in companies]
        }
        return output["companies"]

    def post(self):
        params = request.args.get('company_id', None)
        company_id = self.data['company_id']
        products = self.data['products']

        if CompanyModel.query.filter_by(company_id=company_id).first():
            return {
                "message": f"Company id {company_id} already exists"
            }, 400

        company = CompanyModel.query.filter_by(company_id=params).first()
        if company is None:
            company = CompanyModel(company_id=company_id)

        try:
            for p in products:
                product = ProductModel.query.filter_by(id=p['id']).first()

                if not product:
                    product = ProductModel(id=p['id'], value=p['value'])
                    product.save_model()

                company.products.append(product)

            company.save_model()
        except Exception:
            return {
                "message": "An error ocurred trying to create company."
            }, 500

        return company.json(), 201

    def put(self):
        params = request.args.get('company_id', None)
        company_id = self.data['company_id']
        products = self.data['products']

        company = CompanyModel.query.filter_by(company_id=params).first()
        if company is None:
            company = CompanyModel(company_id=company_id)

        try:
            for p in products:
                product = ProductModel.query.filter_by(id=p['id']).first()

                if not product:
                    product = ProductModel(id=p['id'], value=p['value'])
                else:
                    product.id = p['id']
                    product.value = p['value']

                product.save_model()
                company.products.append(product)

            company.save_model()
        except Exception:
            return {
                "message": "An error ocurred trying to update company."
            }, 500

        return company.json(), 201

    def delete(self):
        params = request.args.get('company_id', None)
        company = CompanyModel.query.filter_by(company_id=params).first()
        if company:
            company.delete_model()

        return {
            "message": "Company deleted."
        }, 204
