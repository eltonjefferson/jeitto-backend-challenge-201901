FROM python:3.8.1-slim-buster

WORKDIR /api

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

COPY ./requirements.txt /api/requirements.txt

RUN pip install --upgrade pip

RUN pip install -r /api/requirements.txt

COPY . /api/

CMD ["python", "api.py"] 
