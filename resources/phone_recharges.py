from flask_restful import Resource, reqparse
from flask import request

from models.phone_recharges import PhoneRecharges
from models.company import CompanyModel
from models.product import ProductModel


class PhoneRechargesResource(Resource):

    def get(self):
        phone_id = request.args.get('id', None)
        phone_number = request.args.get('phone_number', None)

        if phone_id:
            phones = PhoneRecharges.query.filter_by(id=phone_id).first()

        elif phone_number:
            phones = PhoneRecharges.query.filter_by(phone_number=phone_number).all()

        else:
            phones = PhoneRecharges.query.order_by(PhoneRecharges.id).all()

        output = {
            "phones": [c.json() for c in phones]
        }
        return output["phones"]

    def post(self):
        data = self.data()
        company_id = data['company_id']
        product_id = data['product_id']
        phone_number = data['phone_number']
        value = data['value']

        if not CompanyModel.query.filter_by(company_id=company_id).first():
            return {
                "message": f"Company id {company_id} not exists"
            }, 400

        if not ProductModel.query.filter_by(id=product_id).first():
            return {
                "message": f"Product id {product_id} not exists"
            }, 400

        recharges = PhoneRecharges(
            company_id=company_id,
            product_id=product_id,
            phone_number=phone_number,
            value=value,
        )

        try:
            recharges.save_model()
            return recharges.json(), 201
        except Exception:
            return {
                "message": "An error ocurred trying to create phone recharges."
            }, 500

    def data(self):
        payload = reqparse.RequestParser()
        payload.add_argument(
            'id',
            type=str,
            required=False,
            help='No phone id provided',
            location='json'
        )
        payload.add_argument(
            'phone_number',
            type=str,
            required=False,
            help='No phone_number provided',
            location='json'
        )
        payload.add_argument(
            'company_id',
            type=str,
            required=False,
            help='No company_id of phone recharges provided',
            location='json'
        )
        payload.add_argument(
            'product_id',
            type=str,
            required=False,
            help='No product_id of phone recharges provided',
            location='json'
        )
        payload.add_argument(
            'value',
            type=float,
            required=False,
            help='No value  of phone recharges provided',
            location='json'
        )
        return payload.parse_args()
