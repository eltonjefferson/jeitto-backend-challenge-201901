from database import db


class ProductModel(db.Model):
    __tablename__ = "products"

    id = db.Column(db.String, primary_key=True)
    value = db.Column(db.Float(precision=2), nullable=False)
    company_id = db.Column(
        db.String,
        db.ForeignKey('companies.company_id'),
        nullable=True
    )

    def json(self):
        return {
            "id": self.id,
            "value": self.value,
        }

    def save_model(self):
        db.session.add(self)
        db.session.commit()
