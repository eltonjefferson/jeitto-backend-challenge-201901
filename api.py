import os
from dotenv import find_dotenv, load_dotenv

from flask import Flask
from flask_restful import Api

from resources.company_products import CompanyProducts
from resources.phone_recharges import PhoneRechargesResource


load_dotenv(find_dotenv())

app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = os.getenv("DATABASE_URL")
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

api = Api(app)


@app.before_first_request
def create_db():
    from database import db
    db.init_app(app)
    db.create_all()


api.add_resource(CompanyProducts, '/company/products')
api.add_resource(PhoneRechargesResource, "/phone/recharges")
