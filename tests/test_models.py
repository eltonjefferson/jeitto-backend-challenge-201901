from models.company import CompanyModel
from models.product import ProductModel


def test_create_company(data_company):
    company = CompanyModel(
        company_id=data_company['company_id'],
    )

    assert company.company_id == 'claro_11'


def test_company_infos(data_company):
    company = CompanyModel(company_id=data_company['company_id'])

    json = {
        "company_id": company.company_id,
        "products": company.products
    }

    assert company.json() == json


def test_create_product(data_products):
    product = ProductModel(**data_products)

    assert product.id == 'claro_10'


def test_product_infos(data_products):
    product = ProductModel(**data_products)

    json = {
        "id": product.id,
        "value": product.value
    }

    assert product.json() == json
