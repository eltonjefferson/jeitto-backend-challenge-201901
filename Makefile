flake:
	flake8 -v --ignore=E501 .

run:
	docker-compose up -d --build

test:
	docker-compose run api pytest
