import pytest


@pytest.fixture
def data_company():
    return {
        "company_id": "claro_11",
        "products": [
            {"id": "claro_10", "value": 10.0},
        ]
    }


@pytest.fixture
def data_products():
    return {
        "id": "claro_10",
        "value": 10.0,
    }
