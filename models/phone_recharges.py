from database import db
from datetime import datetime


class PhoneRecharges(db.Model):
    __tablename__ = "phone_recharges"

    id = db.Column(db.Integer, primary_key=True)
    created_at = db.Column(db.DateTime, default=datetime.utcnow)
    company_id = db.Column(db.String)
    product_id = db.Column(db.String)
    phone_number = db.Column(db.String)
    value = db.Column(db.Float(precision=2), nullable=False)

    def json(self):
        created_at = self.created_at.strftime("%Y/%m/%dT%H:%M:%S%fZ")
        return {
            "id": self.id,
            "created_at": created_at,
            "company_id": self.company_id,
            "product_id": self.product_id,
            "phone_number": self.phone_number,
            "value": self.value
        }

    def save_model(self):
        db.session.add(self)
        db.session.commit()
