from database import db
from models.product import ProductModel


class CompanyModel(db.Model):
    __tablename__ = "companies"

    company_id = db.Column(db.String, primary_key=True)
    products = db.relationship(ProductModel, backref='companies', lazy=True)

    def json(self):
        return {
            "company_id": self.company_id,
            "products": [p.json() for p in self.products]
        }

    def save_model(self):
        db.session.add(self)
        db.session.commit()

    def delete_model(self):
        db.session.delete(self)
        db.session.commit()
